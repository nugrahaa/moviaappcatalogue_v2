package com.dicoding.picodiploma.academy;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class MovieDetail extends AppCompatActivity {
    private static final String TAG = "MovieDetail";
    private TextView Judul, Tanggal, Sinopsis, User_Score;
    private ImageView Photo;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_movie_and_tvshow);

        getIncomingIntent();
        setImage();

    }

    private void getIncomingIntent() {
        Judul = findViewById(R.id.tv_item_judul_detail);
        Tanggal = findViewById(R.id.tv_item_tanggal_detail);
        Sinopsis = findViewById(R.id.tv_item_sinopsis_detail);
        User_Score = findViewById(R.id.tv_item_userscore_detail);
        Photo = findViewById(R.id.img_item_photo_detail);
    }

    private void setImage() {

        Intent data = getIntent();
        String judul = data.getStringExtra("judul");
        String sinopsis = data.getStringExtra("sinopsis");
        String tanggal = data.getStringExtra("tanggal");
        String score = data.getStringExtra("score");
        String foto = data.getStringExtra("image_url");

        Judul.setText(judul);
        Sinopsis.setText(sinopsis);
        Tanggal.setText(tanggal);
        User_Score.setText(score);

        Glide.with(this)
                .load(getResources().getIdentifier(foto, "drawable", getPackageName()))
                .apply(new RequestOptions().override(55, 55))
                .into(Photo);
    }
}
