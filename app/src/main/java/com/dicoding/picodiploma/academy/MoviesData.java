package com.dicoding.picodiploma.academy;

import java.util.ArrayList;

public class MoviesData {
    public static String[][] data = new String[][] {
            {"A Star Is Born", "October 3, 2018", "Seasoned musician Jackson Maine discovers — and falls in love with — struggling artist Ally. She has just about given up on her dream to make it big as a singer — until Jack coaxes her into the spotlight. But even as Ally's career takes off, the personal side of their relationship is breaking down, as Jack fights an ongoing battle with his own internal demons.", "75%", "poster_a_start_is_born"},
            {"Alita: Battle Angel", "January 31, 2019", "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past.", "68%", "poster_alita"},
            {"Aquaman", "December 7, 2018", "Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne.", "68%", "poster_aquaman"},
            {"Bohemian Rhapsody", "October 24, 2018", "Singer Freddie Mercury, guitarist Brian May, drummer Roger Taylor and bass guitarist John Deacon take the music world by storm when they form the rock 'n' roll band Queen in 1970. Hit songs become instant classics. When Mercury's increasingly wild lifestyle starts to spiral out of control, Queen soon faces its greatest challenge yet – finding a way to keep the band together amid the success and excess.", "80%", "poster_bohemian"},
            {"Creed", "November 25, 2015", "The former World Heavyweight Champion Rocky Balboa serves as a trainer and mentor to Adonis Johnson, the son of his late friend and former rival Apollo Creed.", "73%", "poster_creed"},
            {"Glass", "January 16, 2019", "In a series of escalating encounters, former security guard David Dunn uses his supernatural abilities to track Kevin Wendell Crumb, a disturbed man who has twenty-four personalities. Meanwhile, the shadowy presence of Elijah Price emerges as an orchestrator who holds secrets critical to both men.", "65%", "poster_glass"},
            {"How to Train Your Dragon: The Hidden World", "January 3, 2019", "As Hiccup fulfills his dream of creating a peaceful dragon utopia, Toothless’ discovery of an untamed, elusive mate draws the Night Fury away. When danger mounts at home and Hiccup’s reign as village chief is tested, both dragon and rider must make impossible decisions to save their kind.", "77%", "poster_how_to_train"},
            {"Avengers: Infinity War", "April 25, 2018", "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.", "83%", "poster_infinity_war"},
            {"Robin Hood", "November 21, 2018", "A war-hardened Crusader and his Moorish commander mount an audacious revolt against the corrupt English crown.", "58%", "poster_robin_hood"},
            {"Spider-Man: Far from Home", "June 28, 2019", "Peter Parker and his friends go on a summer trip to Europe. However, they will hardly be able to rest - Peter will have to agree to help Nick Fury uncover the mystery of creatures that cause natural disasters and destruction throughout the continent.", "77%", "poster_spiderman"}
    };

    public static ArrayList<Movie> getListData() {
        ArrayList<Movie> list = new ArrayList<>();

        for (String[] aData : data) {
            Movie movie = new Movie();
            movie.setJudul(aData[0]);
            movie.setTanggal(aData[1]);
            movie.setSinopsis(aData[2]);
            movie.setUser_score(aData[3]);
            movie.setPhoto(aData[4]);

            list.add(movie);
        }

        return list;
    }
}
