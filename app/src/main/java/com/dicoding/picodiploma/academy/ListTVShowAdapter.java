package com.dicoding.picodiploma.academy;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class ListTVShowAdapter extends RecyclerView.Adapter<ListTVShowAdapter.ListViewHolder> {

    private ArrayList<TVShow> listTVShow;

    public ListTVShowAdapter(ArrayList<TVShow> list) {
        this.listTVShow = list;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_tvshow, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        final TVShow tvShow = listTVShow.get(position);

        Context ctx = holder.itemView.getContext();
        Glide.with(ctx)
                .load(ctx.getResources().getIdentifier(tvShow.getPhoto(), "drawable", ctx.getPackageName()))
                .apply(new RequestOptions().override(55,55))
                .into(holder.imgPhoto);

        holder.tvJudul.setText(tvShow.getJudul());
        holder.tvSinopsis.setText(tvShow.getSinopsis());

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), MovieDetail.class);
                intent.putExtra("image_url", tvShow.getPhoto());
                intent.putExtra("judul", tvShow.getJudul());
                intent.putExtra("sinopsis", tvShow.getSinopsis());
                intent.putExtra("tanggal", tvShow.getTanggal());
                intent.putExtra("score", tvShow.getUser_score());

                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listTVShow.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvJudul, tvSinopsis;
        RelativeLayout parentLayout;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_item_photo_tvshow);
            tvJudul = itemView.findViewById(R.id.tv_item_judul_tvshow);
            tvSinopsis = itemView.findViewById(R.id.tv_item_sinopsis_tvshow);
            parentLayout = itemView.findViewById(R.id.parent_layout_tvshow);

        }
    }
}
