package com.dicoding.picodiploma.academy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class ListMovieAdapter extends RecyclerView.Adapter<ListMovieAdapter.ListViewHolder> {

    private ArrayList<Movie> listMovie;

    public ListMovieAdapter(ArrayList<Movie> list) {
        this.listMovie = list;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_movies, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        final Movie movie = listMovie.get(position);

        Context ctx = holder.itemView.getContext();
        Glide.with(ctx)
                .load(ctx.getResources().getIdentifier(movie.getPhoto(), "drawable", ctx.getPackageName()))
                .apply(new RequestOptions().override(55,55))
                .into(holder.imgPhoto);

        holder.tvJudul.setText(movie.getJudul());
        holder.tvSinopsis.setText(movie.getSinopsis());

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), MovieDetail.class);
                intent.putExtra("image_url", movie.getPhoto());
                intent.putExtra("judul", movie.getJudul());
                intent.putExtra("sinopsis", movie.getSinopsis());
                intent.putExtra("tanggal", movie.getTanggal());
                intent.putExtra("score", movie.getUser_score());

                holder.itemView.getContext().startActivity(intent);
            }
        });
        
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        ImageView imgPhoto;
        TextView tvJudul, tvSinopsis;
        RelativeLayout parentLayout;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_item_photo);
            tvJudul = itemView.findViewById(R.id.tv_item_judul);
            tvSinopsis = itemView.findViewById(R.id.tv_item_sinopsis);
            parentLayout = itemView.findViewById(R.id.parent_layout_movie);
        }
    }
}
