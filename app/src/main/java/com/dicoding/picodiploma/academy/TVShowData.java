package com.dicoding.picodiploma.academy;

import java.util.ArrayList;

public class TVShowData {

    public static String[][] data = new String[][] {
            {"Arrow", "October 10, 2012", "Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.", "58%", "poster_arrow"},
            {"Doom Patrol", "February 15, 2019", "The Doom Patrol’s members each suffered horrible accidents that gave them superhuman abilities — but also left them scarred and disfigured. Traumatized and downtrodden, the team found purpose through The Chief, who brought them together to investigate the weirdest phenomena in existence — and to protect Earth from what they find.", "64%", "poster_doom_patrol"},
            {"DragonBall Z", "April 26, 1989", "Dragon Ball Z is a Japanese animated television series produced by Toei Animation. Dragon Ball Z is the sequel to the Dragon Ball anime and adapts the last 26 volumes of the original 42 volume Dragon Ball manga series created by Akira Toriyama The series Debut in 1988-1995 on Weekly Shounen Jump. Dragon Ball Z depicts the continuing adventures of Goku and his companions to defend against an assortment of villains which seek to destroy or rule the Earth.", "80%", "poster_dragon_ball"},
            {"Fairy Tail", "October 12, 2009", "Lucy is a 17-year-old girl, who wants to be a full-fledged mage. One day when visiting Harujion Town, she meets Natsu, a young man who gets sick easily by any type of transportation. But Natsu isn't just any ordinary kid, he's a member of one of the world's most infamous mage guilds: Fairy Tail.", "65%", "poster_fairytail"},
            {"Family Guy", "January 31, 1999", "Sick, twisted, politically incorrect and Freakin' Sweet animated series featuring the adventures of the dysfunctional Griffin family. Bumbling Peter and long-suffering Lois have three kids. Stewie (a brilliant but sadistic baby bent on killing his mother and taking over the world), Meg (the oldest, and is the most unpopular girl in town) and Chris (the middle kid, he's not very bright but has a passion for movies). The final member of the family is Brian - a talking dog and much more than a pet, he keeps Stewie in check whilst sipping Martinis and sorting through his own life issues.", "65%", "poster_family_guy"},
            {"The Flash", "October 7, 2014", "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.", "67%", "poster_flash"},
            {"Marvel's Iron Fist", "March 17, 2017", "Danny Rand resurfaces 15 years after being presumed dead. Now, with the power of the Iron Fist, he seeks to reclaim his past and fulfill his destiny.", "61%", "poster_iron_fist"},
            {"Gotham", "September 22, 2014", "Everyone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker?", "68%", "poster_gotham"},
            {"Grey's Anatomy", "March 27, 2005", "Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital.", "63%", "poster_grey_anatomy"},
            {"Hanna", "March 28, 2019", "This thriller and coming-of-age drama follows the journey of an extraordinary young girl as she evades the relentless pursuit of an off-book CIA agent and tries to unearth the truth behind who she is. Based on the 2011 Joe Wright film.", "66%", "poster_hanna"}
    };

    public static ArrayList<TVShow> getListData() {
        ArrayList<TVShow> list = new ArrayList<>();

        for (String[] aData: data) {
            TVShow tvshow = new TVShow();
            tvshow.setJudul(aData[0]);
            tvshow.setTanggal(aData[1]);
            tvshow.setSinopsis(aData[2]);
            tvshow.setUser_score(aData[3]);
            tvshow.setPhoto(aData[4]);

            list.add(tvshow);
        }

        return list;
    }

}
