package com.dicoding.picodiploma.academy;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class TvShowFragment extends Fragment {

    private RecyclerView rvTvShow;
    private ArrayList<TVShow> list = new ArrayList<>();


    public TvShowFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tv_show, container, false);
        list.addAll(TVShowData.getListData());
        rvTvShow = view.findViewById(R.id.rv_tvshow);
        rvTvShow.setLayoutManager(new LinearLayoutManager(getActivity()));

        ListTVShowAdapter listTVShowAdapter = new ListTVShowAdapter(list);
        rvTvShow.setAdapter(listTVShowAdapter);

        return view;
    }

}
